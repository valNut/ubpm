TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
INCLUDEPATH	+= ../../../../ ../../../shared/hidapi
SOURCES		= DialogImport.cpp hem-7322u.cpp
HEADERS		= DialogImport.h   hem-7322u.h
FORMS		= DialogImport.ui
RESOURCES	= res/hem-7322u.qrc
TRANSLATIONS	= res/qm/hem-7322u-de_DE.ts
TARGET		= ../../../omron-hem7322u

unix:!macx {
SOURCES		+= ../../../shared/hidapi/hidlin.c
LIBS		+= -ludev
}

win32 {
SOURCES		+= ../../../shared/hidapi/hidwin.c
LIBS		+= -lsetupapi
CONFIG		-= debug_and_release
}

macx {
SOURCES		+= ../../../shared/hidapi/hidmac.c
}
