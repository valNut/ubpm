#ifndef DEVICEPLUGIN_H
#define DEVICEPLUGIN_H

#include <QtPlugin>
#include <QObject>

#include "DialogImport.h"

class DevicePlugin : public QObject, DeviceInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "de.lazyt.ubpm.deviceinterface")
	Q_INTERFACES(DeviceInterface)

public:

	DEVICEINFO getDeviceInfo();
	bool getDeviceData(QWidget*, QString, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*);
};

#endif // DEVICEPLUGIN_H
