TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
DEFINES		+= HEM7131U
INCLUDEPATH	+= ../../../../ ../../../shared/hidapi
SOURCES		= ../hem-7322u/DialogImport.cpp ../hem-7322u/hem-7322u.cpp
HEADERS		= ../hem-7322u/DialogImport.h   ../hem-7322u/hem-7322u.h
FORMS		= ../hem-7322u/DialogImport.ui
RESOURCES	= res/hem-7131u.qrc
TARGET		= ../../../omron-hem7131u

unix:!macx {
SOURCES		+= ../../../shared/hidapi/hidlin.c
LIBS		+= -ludev
}

win32 {
SOURCES		+= ../../../shared/hidapi/hidwin.c
LIBS		+= -lsetupapi
CONFIG		-= debug_and_release
}

macx {
SOURCES		+= ../../../shared/hidapi/hidmac.c
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../hem-7322u/res/qm/hem-7322u-de_DE.qm res/qm/hem-7131u-de_DE.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../hem-7322u/res/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/res/qm/hem-7131u-de_DE.qm
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
