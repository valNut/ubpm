#!/bin/sh

TMP=/tmp/ubpm-help

[ -d $TMP ] && rm -r $TMP && mkdir $TMP

# translate html

static-i18n -l en -i en --localesPath=html/lng --output-dir=$TMP/en html
static-i18n -l de -i de --localesPath=html/lng --output-dir=$TMP/de html

# generate qthelp

cp -r html/css html/img $TMP/en
cp -r html/css html/img $TMP/de

cp $TMP/en/img/en/*.png $TMP/en/img
cp $TMP/de/img/de/*.png $TMP/de/img

cp help/en.qh* $TMP/en
cp help/de.qh* $TMP/de

qhelpgenerator $TMP/en/en.qhp  -o $TMP/en.qch
qhelpgenerator $TMP/en/en.qhcp -o $TMP/en.qhc
qhelpgenerator $TMP/de/de.qhp  -o $TMP/de.qch
qhelpgenerator $TMP/de/de.qhcp -o $TMP/de.qhc

cp $TMP/*.q* .
