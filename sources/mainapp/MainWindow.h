#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define APPNAME QObject::tr("Universal Blood Pressure Manager")

#include <QMainWindow>
#include <QKeyEvent>
#include <QMessageBox>
#include <QDir>
#include <QTranslator>
#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <QPrinter>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>
#include <QXmlStreamReader>
#include <QStandardPaths>
#include <QPluginLoader>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QHelpContentWidget>
#include <QHelpEngine>

#include "deviceinterface.h"

#include "DialogAnalysis.h"
#include "DialogHelp.h"
#include "DialogRecord.h"
#include "DialogSettings.h"
#include "DialogUpdate.h"
#include "ui_MainWindow.h"

#define FILE_SETTINGS QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/.config/LazyT/ubpm.ini"
#define FILE_DATABASE QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/ubpm.sql"

#define DIR_PLUGINS   QApplication::applicationDirPath() + "/Plugins/"
#define DIR_LANGUAGES QApplication::applicationDirPath() + "/Languages"
#define DIR_THEMES    QApplication::applicationDirPath() + "/Themes"

#define PRINT_HEADER 0.033
#define PRINT_FOOTER 0.025
#define PRINT_MARGIN 5
#define PRINT_RADIUS 5

#define SYSMAX 130
#define SYSMIN 105
#define DIAMAX 85
#define DIAMIN 65
#define BPMMAX 70
#define BPMMIN 50

#define WRNSYS 140
#define WRNDIA 90
#define WRNPPR 50
#define WRNBPM 80

enum { SYS_R1 = 105, SYS_R2 = 120, SYS_R3 = 130, SYS_R4 = 140, SYS_R5 = 160, SYS_R6 = 180 };
enum { DIA_R1 =  65, DIA_R2 =  80, DIA_R3 =  85, DIA_R4 =  90, DIA_R5 = 100, DIA_R6 = 110 };
enum { BPM_R1, BPM_R2, BPM_R3, BPM_R4, BPM_R5, BPM_R6 };

struct SETTINGS
{
	QString language;
	QString style;
	QString theme;
	QString imp, exp;
	QByteArray geometry;
	int mode, range, slide;

	struct
	{
		QString location;
		bool encryption;
		QString password;

	}database;

	struct
	{
		QString gender;
		int agegroup;
		QString name;
		bool addition;
		int birth;
		int height;
		int weight;

	}user[2];

	struct
	{
		QString plugin;

	}device;

	struct
	{
		bool dynamic;
		bool colored;
		bool symbols;
		bool printhr;
		int symbolsize;

		struct
		{
			int sys_max;
			int sys_min;
			int dia_max;
			int dia_min;
			int bpm_max;
			int bpm_min;

		}range[2];

	}chart;

	struct
	{
		int warnsys;
		int warndia;
		int warnbpm;
		int warnppr;

	}table[2];

	struct
	{
		bool median;
		bool legend;

	}stats;

	struct
	{
		bool autostart;
		bool notification;

	}update;

};

class tableStyledItemDelegate : public QStyledItemDelegate
{
	QString displayText(const QVariant &value, const QLocale &) const
	{
		if(value.type() == QVariant::Date)
		{
			return value.toDate().toString("dd.MM.yyyy");
		}
		else if(value.type() == QVariant::Time)
		{
			return value.toTime().toString("hh:mm:ss");
		}
		else
		{
			return value.toString();
		}
	}

	void setEditorData(QWidget *editor, const QModelIndex &index) const
	{
		if(index.column() == 8)
		{
			QLineEdit *lineedit = dynamic_cast <QLineEdit*>(editor);

			lineedit->setAlignment(Qt::AlignHCenter);
			lineedit->setText(index.data().toString());
		}
		else
		{
			QSpinBox *spinbox = dynamic_cast <QSpinBox*>(editor);

			spinbox->setAlignment(Qt::AlignHCenter);
			spinbox->setValue(index.data().toInt());

			if(index.column() == 6 || index.column() == 7)
			{
				spinbox->setRange(0, 1);
			}
			else
			{
				spinbox->setRange(1, 255);
			}
		}
	}
};

class MainWindow : public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:

	explicit MainWindow(QWidget *parent = nullptr);

	QTranslator translatorApplication;

	QByteArray encryptString(QString);
	QString decryptString(QByteArray);

	bool forced = false;

	bool recordAdd(bool, HEALTHDATA);
	bool recordMod(bool, HEALTHDATA);
	bool recordDel(bool, qint64);
	bool recordHide(bool, qint64, bool);

	void switchUser(bool);
    QString importFromFile(QString*);
    void saveDatabase();

private:

	int bpm_target_areas[2][6][6] = { {{ 56, 55, 57, 58, 57, 56 }, { 62, 62, 63, 64, 62, 62 }, { 66, 66, 67, 68, 68, 66 }, { 70, 71, 71, 72, 72, 70 }, { 74, 75, 76, 77, 76, 74 }, { 82, 82, 83, 84, 82, 80 }}, {{ 61, 60, 60, 61, 60, 60 }, { 66, 65, 65, 66, 65, 65 }, { 70, 69, 70, 70, 69, 69 }, { 74, 73, 74, 74, 74, 73 }, { 79, 77, 79, 78, 78, 77 }, { 85, 83, 85, 84, 84, 85 }} };

	QString syscolors[7] = { "#68a4fc", "#458ffc", "#227afb", "#0366f5", "#0357d2", "#0249af", "#023a8c" };
	QString diacolors[7] = { "#8dee76", "#73ea56", "#58e736", "#40df1a", "#37bf16", "#2e9f13", "#257f0f" };
	QString bpmcolors[7] = { "#ed7977", "#e95957", "#e53a37", "#dd1f1b", "#be1a17", "#9e1613", "#7e110f" };

	SETTINGS settings;

	QVector <HEALTHDATA> database[2];

	QVector <QPluginLoader*> plugins;
	int plugin;

	struct
	{
		qint64 init, exit;
		QVector<HEALTHDATA> data;
		int user = 0;
		int records;
		int SYSmin, SYSmax, SYSavg, SYSmed;
		int DIAmin, DIAmax, DIAavg, DIAmed;
		int BPMmin, BPMmax, BPMavg, BPMmed;
		int SYSR1, SYSR2, SYSR3, SYSR4, SYSR5, SYSR6, SYSR7;
		int DIAR1, DIAR2, DIAR3, DIAR4, DIAR5, DIAR6, DIAR7;
		int BPMR1, BPMR2, BPMR3, BPMR4, BPMR5, BPMR6, BPMR7;

	}view;

	QSqlDatabase sqldb;
	bool sqlcipher;

	QTranslator translatorQtBase;

	QWidget *toolbarSpacer = new QWidget();
	QDateTimeEdit *toolbarDateTime = new QDateTimeEdit(QDateTime(QDate::currentDate(), QTime(0, 0, 0, 0)));

	QLCDNumber *lcd = new QLCDNumber(1);

	QLabel *statusbarLabel = new QLabel();

	QString theme;

	QChart *chartMainBP = new QChart();
	QChart *chartMainHR = new QChart();
	QChart *chartSYS1 = new QChart();
	QChart *chartDIA1 = new QChart();
	QChart *chartBPM1 = new QChart();
	QChart *chartSYS2 = new QChart();
	QChart *chartDIA2 = new QChart();
	QChart *chartBPM2 = new QChart();

	QDateTimeAxis *axisXBP = new QDateTimeAxis();
	QValueAxis *axisY1BP = new QValueAxis();
	QValueAxis *axisY2BP = new QValueAxis();

	QDateTimeAxis *axisXHR = new QDateTimeAxis();
	QValueAxis *axisY1HR = new QValueAxis();
	QValueAxis *axisY2HR = new QValueAxis();

	QBarSet *setSYS1Min = new QBarSet(tr("Minimum"));
	QBarSet *setSYS1Max = new QBarSet(tr("Maximum"));
	QBarSet *setSYS1Avg = new QBarSet(tr("Average"));
	QBarSet *setSYS1Med = new QBarSet(tr("Median"));
	QBarSet *setDIA1Min = new QBarSet(tr("Minimum"));
	QBarSet *setDIA1Max = new QBarSet(tr("Maximum"));
	QBarSet *setDIA1Avg = new QBarSet(tr("Average"));
	QBarSet *setDIA1Med = new QBarSet(tr("Median"));
	QBarSet *setBPM1Min = new QBarSet(tr("Minimum"));
	QBarSet *setBPM1Max = new QBarSet(tr("Maximum"));
	QBarSet *setBPM1Avg = new QBarSet(tr("Average"));
	QBarSet *setBPM1Med = new QBarSet(tr("Median"));

	QBarSeries *seriesSYS1 = new QBarSeries();
	QBarSeries *seriesDIA1 = new QBarSeries();
	QBarSeries *seriesBPM1 = new QBarSeries();

	QPieSeries *seriesSYS2 = new QPieSeries();
	QPieSeries *seriesDIA2 = new QPieSeries();
	QPieSeries *seriesBPM2 = new QPieSeries();

	QScatterSeries *scatterSeriesSYS = new QScatterSeries();
	QScatterSeries *scatterSeriesDIA = new QScatterSeries();
	QScatterSeries *scatterSeriesBPM = new QScatterSeries();
	QScatterSeries *scatterSeriesIHB = new QScatterSeries();

	QLineSeries *lineSeriesSYS = new QLineSeries();
	QLineSeries *lineSeriesDIA = new QLineSeries();
	QLineSeries *lineSeriesBPM = new QLineSeries();

	QLineSeries *lineSeriesUpperSYS = new QLineSeries();
	QLineSeries *lineSeriesLowerSYS = new QLineSeries();
	QAreaSeries *areaSeriesSYS = new QAreaSeries(lineSeriesUpperSYS, lineSeriesLowerSYS);
	QLineSeries *lineSeriesUpperDIA = new QLineSeries();
	QLineSeries *lineSeriesLowerDIA = new QLineSeries();
	QAreaSeries *areaSeriesDIA = new QAreaSeries(lineSeriesUpperDIA, lineSeriesLowerDIA);
	QLineSeries *lineSeriesUpperBPM = new QLineSeries();
	QLineSeries *lineSeriesLowerBPM = new QLineSeries();
	QAreaSeries *areaSeriesBPM = new QAreaSeries(lineSeriesUpperBPM, lineSeriesLowerBPM);

	QDateTime timespan = toolbarDateTime->dateTime();

	QPrintPreviewDialog *printPreviewDialog;
	QPrintDialog *printDialog;

	int lcdDigitWidth;

	void clearUser(int);

	void readSettings();
	void saveSettings();

	void scanPlugins();
	void scanLanguages();
	void scanStyles();
	void scanThemes();

	void hideAxisLabels(QChartView*);

	void updateLCD();

	void calcView();

	void showChart();
	void showTable();
	void showStats();

	int calculatePrintTableRowHeight(QTextDocument*);

	void printPreview(int);
	void printRecords(int);

	void printChart();
	void printTable();
	void printStats();

	void prepareMainChart(QChart*, QChartView*, QDateTimeAxis*, QValueAxis*, QValueAxis*, int, int);
	void prepareAreaChart(QChart*, QDateTimeAxis*, QValueAxis*, QAreaSeries*, QLineSeries*, QLineSeries*, QColor, int, int);
	void prepareLineChart(QChart*, QDateTimeAxis*, QValueAxis*, QLineSeries*, QColor, QString);
	void prepareScatterChart(QChart*, QDateTimeAxis*, QValueAxis*, QScatterSeries*);
	void prepareBarChart(QChartView*, QChart*, QBarSeries*, QBarSet*, QBarSet*, QBarSet*, QBarSet*, QString);
	void preparePieChart(QChartView*, QChart*, QPieSeries*, QString);

	void modifyChartRanges();

	void drawBarChart(QChart*, QBarSeries*, QBarSet*, QBarSet*, QBarSet*, QBarSet*, int, int, int, int);
	void drawPieChart(QChart*, QPieSeries*, int, int, int, int, int, int, int);

	int validateDB();

	void importFromFile();
	int importFromCSV(QFile*, int*, int*);
	int importFromXML(QFile*, int*, int*);
	int importFromJSON(QFile*, int*, int*);
	int importFromSQL(QString, int*, int*);

	void exportToFile(QString);
	void exportToCSV(QFile*);
	void exportToXML(QFile*);
	void exportToJSON(QFile*);
	void exportToSQL(QString);

	void changeTimeAxis(QString, int, int, int);

private slots:

	void initAfterShown();

	void chartAxisXRangeChanged();

	void chartSeriesClicked(QPointF);
	void chartSeriesHovered(QPointF, bool);
	void barSeriesClicked(int, QBarSet*);
	void barSeriesHovered(bool, int, QBarSet*);
	void pieSeriesClicked(QPieSlice*);
	void pieSeriesHovered(QPieSlice*, bool);
	void pieLegendClicked();
	void pieLegendHovered(bool);

	void languageChanged(QAction*);
	void themeChanged(QAction*);
	void styleChanged(QAction*);

	void toolbarDateTimeChanged(QDateTime);

	void tableItemChanged(QTableWidgetItem*);

	void on_actionPreviewChart_triggered();
	void on_actionPreviewTable_triggered();
	void on_actionPreviewStatistic_triggered();
	void on_actionPrintChart_triggered();
	void on_actionPrintTable_triggered();
	void on_actionPrintStatistic_triggered();
	void on_actionImportDevice_triggered();
	void on_actionImportFile_triggered();
	void on_actionImportInput_triggered();
	void on_actionExportCSV_triggered();
	void on_actionExportXML_triggered();
	void on_actionExportJSON_triggered();
	void on_actionExportSQL_triggered();
	void on_actionClearAll_triggered();
	void on_actionClearUser1_triggered();
	void on_actionClearUser2_triggered();
	void on_actionAnalysis_triggered();
	void on_actionSettings_triggered();
	void on_actionSwitchUser1_triggered(bool);
	void on_actionSwitchUser2_triggered(bool);
	void on_actionTimeMode_triggered(bool);
	void on_actionQuit_triggered();
	void on_actionAbout_triggered();
	void on_actionGuide_triggered();
	void on_actionBugreport_triggered();
	void on_actionDonateAmazon_triggered();
	void on_actionDonateLiberapay_triggered();
	void on_actionDonatePaypal_triggered();
	void on_actionDonateSEPA_triggered();
	void on_actionUpdate_triggered();

	void on_toolButton_prev_clicked();
	void on_toolButton_next_clicked();

	void on_toolButton_15m_toggled(bool);
	void on_toolButton_30m_toggled(bool);
	void on_toolButton_hourly_toggled(bool);
	void on_toolButton_6h_toggled(bool);
	void on_toolButton_12h_toggled(bool);
	void on_toolButton_daily_toggled(bool);
	void on_toolButton_weekly_toggled(bool);
	void on_toolButton_monthly_toggled(bool);
	void on_toolButton_quarterly_toggled(bool);
	void on_toolButton_halfyearly_toggled(bool);
	void on_toolButton_yearly_toggled(bool);

	void on_toolButton_days7_toggled(bool);
	void on_toolButton_days14_toggled(bool);
	void on_toolButton_days21_toggled(bool);
	void on_toolButton_days28_toggled(bool);
	void on_toolButton_months3_toggled(bool);
	void on_toolButton_months6_toggled(bool);
	void on_toolButton_months9_toggled(bool);
	void on_toolButton_months12_toggled(bool);
	void on_toolButton_all_toggled(bool);

	void on_tabWidget_currentChanged(int);

	void customContextMenuRequestedChart(QPoint);
	void customContextMenuRequestedTable(QPoint);
	void customContextMenuRequestedStats(QPoint);

	void wheelEvent(QWheelEvent*);
	void keyPressEvent(QKeyEvent*);
	void closeEvent(QCloseEvent*);
};

#endif // MAINWINDOW_H
