<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Daten Analyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Abfrage</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="192"/>
        <source>Results</source>
        <translation>Ergebnisse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Pulsdruck</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Unregelmäßig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Invisible</source>
        <translation>Unsichtbar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="190"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="47"/>
        <source>Could not create memory database!

%1</source>
        <translation>Konnte keine Speicherdatenbank erstellen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="194"/>
        <source>No results for this query found!</source>
        <translation>Keine Ergebnisse für diese Abfrage gefunden!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="200"/>
        <source>Results for %1 [ %2 Matches | %3 Records | %4% ]</source>
        <translation>Ergebnisse für %1 [ %2 Treffer | %3 Datensätze | %4% ]</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 Handbuch nicht gefunden, zeige stattdessen EN Handbuch an.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 Handbuch nicht gefunden!</translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Manueller Datensatz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Datensatz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="227"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Datensatz für %1 hinzufügen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum &amp; Zeit auswählen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Regular Heartbeat</source>
        <translation>Regelmäßiger Herzschlag</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="134"/>
        <source>Enter SYS</source>
        <translation>SYS eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="156"/>
        <source>Enter DIA</source>
        <translation>DIA eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="178"/>
        <source>Enter BPM</source>
        <translation>SPM eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="200"/>
        <source>Irregular Heartbeat</source>
        <translation>Unregelmäßiger Herzschlag</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="255"/>
        <source>Enter Optional Comment</source>
        <translation>Optionalen Kommentar eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="285"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Meldung bei erfolgreichem Erstellen / Löschen / Ändern</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="303"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="320"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="340"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="29"/>
        <location filename="../DialogRecord.cpp" line="97"/>
        <source>Modify</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="45"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Der Datensatz konnte nicht gelöscht werden!

Ein Eintrag für dieses Datum und diese Uhrzeit existiert nicht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="51"/>
        <source>Data record successfully deleted.</source>
        <translation>Datensatz erfolgreich gelöscht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="61"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;SYS&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="71"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;DIA&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="80"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;SPM&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="101"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Der Datensatz konnte nicht geändert werden!

Ein Eintrag für dieses Datum und diese Uhrzeit existiert nicht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="107"/>
        <source>Data Record successfully modified.</source>
        <translation>Datensatz erfolgreich geändert.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="116"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Der Datensatz konnte nicht erstellt werden!

Es existiert bereits ein Eintrag für dieses Datum und diese Uhrzeit.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="122"/>
        <source>Data Record successfully created.</source>
        <translation>Datensatz erfolgreich erstellt.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="122"/>
        <source>Choose Database Location</source>
        <translation>Datenbank-Standort wählen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="158"/>
        <source>Could not display manual!

%1</source>
        <translation>Das Handbuch konnte nicht angezeigt werden!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="269"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Die SQL-Verschlüsselung kann ohne Passwort nicht aktiviert werden und wird deaktiviert!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="276"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Bitte gültige Werte für Zusatzinformationen von Benutzer 1 eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="283"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Bitte gültige Werte für Zusatzinformationen von Benutzer 2 eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="290"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Eingegebenes Alter stimmt nicht mit der gewählten Altersgruppe für Benutzer 1 überein!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="297"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Eingegebenes Alter stimmt nicht mit der gewählten Altersgruppe für Benutzer 2 überein!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="381"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="395"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="521"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Einstellungen abbrechen und alle Änderungen verwerfen?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation>Standort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation>Aktueller Standort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation>Standort ändern</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Mit SQLCipher verschlüsseln</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation>Passwort anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation>Männlich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation>Weiblich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation>Pflichtangaben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation>Altersgruppe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation>Zusatzangaben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation>Import Erweiterungen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin...</source>
        <translation>Bitte Geräte Erweiterung wählen...</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation>Gerätebild anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation>Gerätehandbuch anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation>Webseite öffnen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation>Betreuer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation>E-Mail senden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>X-Axis Range</source>
        <translation>Bereich X-Achse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="912"/>
        <source>Dynamic Scaling</source>
        <translation>Dyn. Skalierung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="931"/>
        <source>Healthy Ranges</source>
        <translation>Gesunde Bereiche</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Symbols</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="954"/>
        <source>Interactive Objects</source>
        <translation>Interaktive Objekte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="967"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="925"/>
        <source>Colored Areas</source>
        <translation>Farbbereiche</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation>Geburtsjahr</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="973"/>
        <source>Include Heartrate</source>
        <translation>Inkl. Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="999"/>
        <location filename="../DialogSettings.ui" line="1274"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1087"/>
        <location filename="../DialogSettings.ui" line="1362"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1175"/>
        <location filename="../DialogSettings.ui" line="1450"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1547"/>
        <source>Table</source>
        <translation>Tabelle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1578"/>
        <location filename="../DialogSettings.ui" line="1819"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolische Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1638"/>
        <location filename="../DialogSettings.ui" line="1879"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolische Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1696"/>
        <location filename="../DialogSettings.ui" line="1937"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulsdruck Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1750"/>
        <location filename="../DialogSettings.ui" line="1991"/>
        <source>Heartrate Warnlevel</source>
        <translation>Herzfrequenz Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2056"/>
        <source>Statistic</source>
        <translation>Statistik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2062"/>
        <source>Bar Type</source>
        <translation>Balken Typ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2068"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Median anstelle Mittelwert Balken anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2078"/>
        <source>Legend Type</source>
        <translation>Legenden Typ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2084"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Werte anstelle von Beschreibungen anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2099"/>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2105"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2111"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Bei Programmstart auf Aktualisierungen prüfen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2124"/>
        <source>Notification</source>
        <translation>Benachrichtigung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2130"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Ergebnis nach Online Prüfung immer anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2146"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2163"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2180"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Online Aktualisierung</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Verfügbare Version</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Aktualisierungsgröße</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Installierte Version</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="159"/>
        <source>No new version found.</source>
        <translation>Keine neue Version gefunden.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="37"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>!!! SSL WARNUNG - SORGFÄLTIG LESEN !!!

Netzwerkverbindungsproblem(e):

%1
Möchten Sie trotzdem fortfahren?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="54"/>
        <source>Downloading update failed!

%1</source>
        <translation>Herunterladen der Aktualisierung fehlgeschlagen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="54"/>
        <source>Checking update failed!

%1</source>
        <translation>Überprüfung der Aktualisierung fehlgeschlagen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="132"/>
        <source>Unexpected response from update server!</source>
        <translation>Unerwartete Antwort vom Aktualisierungsserver!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="179"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download...</source>
        <translation>Aktualisierung hat nicht die erwartete Größe!

%L1 : %L2

Erneut herunterladen...</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Aktualisierung gespeichert nach %1.

Neue Version jetzt starten?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="196"/>
        <source>Could not start new version!</source>
        <translation>Konnte neue Version nicht starten!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="239"/>
        <source>Really abort download?</source>
        <translation>Herunterladen wirklich abbrechen?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Konnte Aktualisierung nicht nach %1 speichern!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universeller Blutdruck Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Diagramm-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabellen-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="75"/>
        <location filename="../MainWindow.cpp" line="3123"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="3124"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulsdruck</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Unregelmäßig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Invisible</source>
        <translation>Unsichtbar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="300"/>
        <source>Statistic View</source>
        <translation>Statistik-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="395"/>
        <source>¼ Hourly</source>
        <translation>¼-Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="430"/>
        <source>½ Hourly</source>
        <translation>½-Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="462"/>
        <source>Hourly</source>
        <translation>Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="497"/>
        <source>¼ Daily</source>
        <translation>¼-Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="529"/>
        <source>½ Daily</source>
        <translation>½-Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="561"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="596"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="628"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="660"/>
        <source>Quarterly</source>
        <translation>Vierteljährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="692"/>
        <source>½ Yearly</source>
        <translation>Halbjährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="724"/>
        <source>Yearly</source>
        <translation>Jährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="784"/>
        <source>Last 7 Days</source>
        <translation>Letzte 7 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="819"/>
        <source>Last 14 Days</source>
        <translation>Letzte 14 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="851"/>
        <source>Last 21 Days</source>
        <translation>Letzte 21 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="883"/>
        <source>Last 28 Days</source>
        <translation>Letzte 28 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="915"/>
        <source>Last 3 Months</source>
        <translation>Letzte 3 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="947"/>
        <source>Last 6 Months</source>
        <translation>Letzte 6 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="979"/>
        <source>Last 9 Months</source>
        <translation>Letzte 9 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1011"/>
        <source>Last 12 Months</source>
        <translation>Letzte 12 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1043"/>
        <source>All Records</source>
        <translation>Alle Datensätze</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1090"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1094"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1114"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1144"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1148"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1166"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1183"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1187"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1199"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1292"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1295"/>
        <location filename="../MainWindow.ui" line="1298"/>
        <source>Quit Program</source>
        <translation>Programm beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>About Program</source>
        <translation>Über das Programm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1322"/>
        <source>Guide</source>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <location filename="../MainWindow.ui" line="1328"/>
        <source>Show Guide</source>
        <translation>Handbuch anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <location filename="../MainWindow.ui" line="1343"/>
        <source>Check Update</source>
        <translation>Aktualisierung prüfen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Donate via Paypal</source>
        <translation>Spenden über Paypal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Donate via Liberapay</source>
        <translation>Spenden über Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <source>Donate via Amazon</source>
        <translation>Spenden über Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <source>Donate via SEPA</source>
        <translation>Spenden über SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1121"/>
        <source>Donation</source>
        <translation>Spende</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3125"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1118"/>
        <source>Make Donation</source>
        <translation>Spende tätigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <source>Bugreport</source>
        <translation>Fehlerbericht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <location filename="../MainWindow.ui" line="1358"/>
        <source>Send Bugreport</source>
        <translation>Fehlerbericht senden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1370"/>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Change Settings</source>
        <translation>Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>From Device</source>
        <translation>Von Gerät</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <location filename="../MainWindow.ui" line="1391"/>
        <source>Import From Device</source>
        <translation>Von Gerät importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <source>From File</source>
        <translation>Von Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1403"/>
        <location filename="../MainWindow.ui" line="1406"/>
        <source>Import From File</source>
        <translation>Von Datei importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>From Input</source>
        <translation>Von Eingabe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1418"/>
        <location filename="../MainWindow.ui" line="1421"/>
        <source>Import From Input</source>
        <translation>Von Eingabe importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <source>To CSV</source>
        <translation>Als CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <location filename="../MainWindow.ui" line="1436"/>
        <source>Export To CSV</source>
        <translation>Als CSV exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>To XML</source>
        <translation>Als XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Export To XML</source>
        <translation>Als XML exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>To JSON</source>
        <translation>Als JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Export To JSON</source>
        <translation>Als JSON exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To SQL</source>
        <translation>Als SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To SQL</source>
        <translation>Als SQL exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>Print Chart</source>
        <translation>Diagramm drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Print Chart View</source>
        <translation>Diagramm-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Print Table</source>
        <translation>Tabelle drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Print Table View</source>
        <translation>Tabellen-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Statistic</source>
        <translation>Statistik drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Statistic View</source>
        <translation>Statistik-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Preview Chart</source>
        <translation>Vorschau Diagramm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Preview Chart View</source>
        <translation>Vorschau Diagramm-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Preview Table</source>
        <translation>Vorschau Tabelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Preview Table View</source>
        <translation>Vorschau Tabellen-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Statistic</source>
        <translation>Vorschau Statistik</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Statistic View</source>
        <translation>Vorschau Statistik-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Clear All</source>
        <translation>Alles löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Clear User 1</source>
        <translation>Benutzer 1 löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear User 2</source>
        <translation>Benutzer 2 löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Switch User 1</source>
        <translation>Benutzer 1 wechseln</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Switch User 2</source>
        <translation>Benutzer 2 wechseln</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1664"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Analyze Records</source>
        <translation>Datensätze analysieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Time Mode</source>
        <translation>Zeit Modus</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="47"/>
        <location filename="../MainWindow.cpp" line="48"/>
        <location filename="../MainWindow.cpp" line="3093"/>
        <location filename="../MainWindow.cpp" line="3094"/>
        <source>Records For Selected User</source>
        <translation>Datensätze für ausgewählten Benutzer</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="55"/>
        <location filename="../MainWindow.cpp" line="56"/>
        <location filename="../MainWindow.cpp" line="3096"/>
        <location filename="../MainWindow.cpp" line="3097"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum &amp; Zeit auswählen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="3127"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisch - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="3128"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisch - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="88"/>
        <location filename="../MainWindow.cpp" line="3130"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisch - Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="89"/>
        <location filename="../MainWindow.cpp" line="3131"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisch - Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1118"/>
        <source>Athlete</source>
        <translation>Sportler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1118"/>
        <source>To Low</source>
        <translation>Zu Niedrig</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1119"/>
        <source>Excellent</source>
        <translation>Hervorragend</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1119"/>
        <source>Optimal</source>
        <translation>Optimal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1120"/>
        <source>Great</source>
        <translation>Sehr gut</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1121"/>
        <source>Good</source>
        <translation>Gut</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1120"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1634"/>
        <location filename="../MainWindow.ui" line="1637"/>
        <location filename="../MainWindow.ui" line="1652"/>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.cpp" line="41"/>
        <location filename="../MainWindow.cpp" line="42"/>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="2963"/>
        <location filename="../MainWindow.cpp" line="2964"/>
        <location filename="../MainWindow.cpp" line="2965"/>
        <location filename="../MainWindow.cpp" line="2966"/>
        <location filename="../MainWindow.cpp" line="3154"/>
        <location filename="../MainWindow.cpp" line="3155"/>
        <location filename="../MainWindow.cpp" line="3156"/>
        <location filename="../MainWindow.cpp" line="3157"/>
        <source>Switch To %1</source>
        <translation>Zu %1 wechseln</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="41"/>
        <location filename="../MainWindow.cpp" line="42"/>
        <location filename="../MainWindow.cpp" line="244"/>
        <location filename="../MainWindow.cpp" line="2963"/>
        <location filename="../MainWindow.cpp" line="2964"/>
        <location filename="../MainWindow.cpp" line="3154"/>
        <location filename="../MainWindow.cpp" line="3155"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="254"/>
        <location filename="../MainWindow.cpp" line="2965"/>
        <location filename="../MainWindow.cpp" line="2966"/>
        <location filename="../MainWindow.cpp" line="3156"/>
        <location filename="../MainWindow.cpp" line="3157"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="86"/>
        <location filename="../MainWindow.cpp" line="3129"/>
        <source>Heartrate - Value Range</source>
        <translation>Herzfrequenz - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="90"/>
        <location filename="../MainWindow.cpp" line="3132"/>
        <source>Heartrate - Target Area</source>
        <translation>Herzfrequenz Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="677"/>
        <source>Measurements : %1  |  Irregular Heartbeat : %2</source>
        <translation>Messwerte : %1  |  Unregelmäßiger Herzschlag : %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="678"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  SPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="682"/>
        <source>Measurements : 0  |  Irregular Heartbeat : 0</source>
        <translation>Messwerte : 0  |  Unregelmäßiger Herzschlag : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="683"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  SPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1121"/>
        <source>High Normal</source>
        <translation>Hoch Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1122"/>
        <location filename="../MainWindow.cpp" line="1587"/>
        <location filename="../MainWindow.cpp" line="3136"/>
        <location filename="../MainWindow.cpp" line="3140"/>
        <location filename="../MainWindow.cpp" line="3144"/>
        <location filename="../MainWindow.h" line="277"/>
        <location filename="../MainWindow.h" line="281"/>
        <location filename="../MainWindow.h" line="285"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1122"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1123"/>
        <source>Below Average</source>
        <translation>Unter Durchschnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1123"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1124"/>
        <source>Poor</source>
        <translation>Schlecht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1124"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1273"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Scannen der Import-Erweiterung &quot;%1&quot; fehlgeschlagen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1286"/>
        <location filename="../MainWindow.cpp" line="1303"/>
        <location filename="../MainWindow.cpp" line="3103"/>
        <source>Switch Language to %1</source>
        <translation>Sprache auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1328"/>
        <location filename="../MainWindow.cpp" line="1343"/>
        <location filename="../MainWindow.cpp" line="3111"/>
        <source>Switch Theme to %1</source>
        <translation>Thema auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1368"/>
        <location filename="../MainWindow.cpp" line="1388"/>
        <location filename="../MainWindow.cpp" line="3119"/>
        <source>Switch Style to %1</source>
        <translation>Stil auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1459"/>
        <location filename="../MainWindow.cpp" line="1484"/>
        <source>Edit record</source>
        <translation>Datensatz bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1587"/>
        <location filename="../MainWindow.cpp" line="3137"/>
        <location filename="../MainWindow.cpp" line="3141"/>
        <location filename="../MainWindow.cpp" line="3145"/>
        <location filename="../MainWindow.h" line="278"/>
        <location filename="../MainWindow.h" line="282"/>
        <location filename="../MainWindow.h" line="286"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1589"/>
        <source>Click to swap Average and Median</source>
        <translation>Klicken zum Wechsel zwischen Mittelwert und Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klicken zum Wechsel zwischen Legende und Beschriftung</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1763"/>
        <source>No records to preview for selected time range!</source>
        <translation>Keine Datensätze für Vorschau im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1797"/>
        <source>No records to print for selected time range!</source>
        <translation>Keine Datensätze zum Drucken im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1833"/>
        <location filename="../MainWindow.cpp" line="1909"/>
        <location filename="../MainWindow.cpp" line="2023"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation>%1 (Alter: %2, Größe: %3, Gewicht: %4)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1833"/>
        <location filename="../MainWindow.cpp" line="1909"/>
        <location filename="../MainWindow.cpp" line="2023"/>
        <source>User %1</source>
        <translation>Benutzer %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>DATE</source>
        <translation>DATUM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>TIME</source>
        <translation>ZEIT</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>BPM</source>
        <translation>SPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>IHB</source>
        <translation>UHS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>COMMENT</source>
        <translation>KOMMENTAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1943"/>
        <source>PPR</source>
        <translation>PDR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3021"/>
        <source>This program is Freeware and may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation>Dieses Programm ist Freeware und darf für den nicht kommerziellen Gebrauch auf beliebig vielen Computern ohne Einschränkungen kostenlos installiert und genutzt werden. Eine Haftung für Schäden, die sich aus der Nutzung ergeben, ist ausgeschlossen. Die Verwendung erfolgt auf eigene Gefahr.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3036"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Bitte kaufen Sie einen Gutschein in der gewünschten Höhe über Ihr Amazon-Konto, wählen Sie als Versandart E-Mail an lazyt@mailbox.org und geben Sie als Nachricht &quot;UBPM&quot; an.

Vielen Dank!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3053"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Bitte senden Sie mir eine E-Mail Anfrage an lazyt@mailbox.org, damit ich Ihnen meine aktuellen Kontodaten mitteilen kann.

Vielen Dank!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3080"/>
        <source>Loading application translation failed!</source>
        <translation>Laden der Anwendungsübersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1827"/>
        <location filename="../MainWindow.cpp" line="1903"/>
        <location filename="../MainWindow.cpp" line="2017"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation>Erstellt mit UBPM für
Windows / Linux / MacOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1828"/>
        <location filename="../MainWindow.cpp" line="1904"/>
        <location filename="../MainWindow.cpp" line="2018"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Kostenlos und OpenSource
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2115"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Import von CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2115"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV Datei (*.csv);;XML Datei (*.xml);;JSON Datei (*.json);;SQL Datei (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2153"/>
        <location filename="../MainWindow.cpp" line="2873"/>
        <source>Successfully imported %1 records from %2.

     User 1 : %3
     User 2 : %4</source>
        <translation>%1 Datensätze erfolgreich von %2 importiert.

     Benutzer 1 : %3
     Benutzer 2 : %4</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2157"/>
        <source>Skipped %1 invalid entries!</source>
        <translation>%1 ungültige Einträge übersprungen!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2162"/>
        <location filename="../MainWindow.cpp" line="2877"/>
        <source>Skipped %1 duplicate entries!</source>
        <translation>%1 doppelte Einträge übersprungen!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2178"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Konnte &quot;%1&quot; nicht öffnen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2503"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Sieht nicht nach einer UBPM-Datenbank aus!

Eventuell falsche Verschlüsselungseinstellungen/Passwort?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2550"/>
        <source>Export to %1</source>
        <translation>Export als %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2550"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Datei (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2581"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Konnte &quot;%1&quot; nicht erzeugen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2587"/>
        <source>The database is empty, no records to export!</source>
        <translation>Die Datenbank ist leer, keine Datensätze zum exportieren!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2735"/>
        <source>Morning</source>
        <translation>Vormittag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2735"/>
        <source>Afternoon</source>
        <translation>Nachmittag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2743"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2751"/>
        <source>Quarter</source>
        <translation>Quartal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2755"/>
        <source>Half Year</source>
        <translation>Halbjahr</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2759"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2807"/>
        <source>Really delete all records for user %1?</source>
        <translation>Wirklich alle Datensätze für Benutzer %1 löschen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2822"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle Datensätze für Benutzer %1 gelöscht und bestehende Datenbank nach &quot;ubpm.sql.bak&quot; gesichert.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2927"/>
        <source>Really delete all records?</source>
        <translation>Wirklich alle Datensätze löschen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2938"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle Datensätze gelöscht und bestehende Datenbank &quot;ubpm.sql&quot; in den Papierkorb verschoben.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3089"/>
        <source>Loading Qt base translation failed!</source>
        <translation>Laden der Qt-Basis Übersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3134"/>
        <location filename="../MainWindow.cpp" line="3138"/>
        <location filename="../MainWindow.cpp" line="3142"/>
        <location filename="../MainWindow.h" line="275"/>
        <location filename="../MainWindow.h" line="279"/>
        <location filename="../MainWindow.h" line="283"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3135"/>
        <location filename="../MainWindow.cpp" line="3139"/>
        <location filename="../MainWindow.cpp" line="3143"/>
        <location filename="../MainWindow.h" line="276"/>
        <location filename="../MainWindow.h" line="280"/>
        <location filename="../MainWindow.h" line="284"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3190"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Konnte Theme &quot;%1&quot; nicht öffnen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1456"/>
        <location filename="../MainWindow.cpp" line="1473"/>
        <location filename="../MainWindow.cpp" line="3785"/>
        <source>Delete record</source>
        <translation>Datensatz löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3787"/>
        <source>Show record</source>
        <translation>Datensatz einblenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1457"/>
        <location filename="../MainWindow.cpp" line="1480"/>
        <location filename="../MainWindow.cpp" line="3788"/>
        <source>Hide record</source>
        <translation>Datensatz ausblenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1475"/>
        <location filename="../MainWindow.cpp" line="3799"/>
        <source>Really delete selected record?</source>
        <translation>Ausgewählten Datensatz wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3734"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamische Skalierung</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3735"/>
        <source>Colored Stripes</source>
        <translation>Farbige Streifen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3736"/>
        <source>Show Symbols</source>
        <translation>Symbole anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3738"/>
        <source>Print Heartrate</source>
        <translation>Herzfrequenz drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3820"/>
        <source>Show Median</source>
        <translation>Median anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3821"/>
        <source>Show Values</source>
        <translation>Werte anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3862"/>
        <source>Really quit program?</source>
        <translation>Programm wirklich beenden?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universeller Blutdruck Manager</translation>
    </message>
</context>
</TS>
