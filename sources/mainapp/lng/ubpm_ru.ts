<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Анализ данных</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Запрос</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="192"/>
        <source>Results</source>
        <translation>Полученные результаты</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>P.Давление</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation type="unfinished">Пульс</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>Нерегулярный</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Invisible</source>
        <translation>Невидимый</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="190"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="47"/>
        <source>Could not create memory database!

%1</source>
        <translation>Не удалось создать базу данных памяти! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="194"/>
        <source>No results for this query found!</source>
        <translation>По этому запросу ничего не найдено!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="200"/>
        <source>Results for %1 [ %2 Matches | %3 Records | %4% ]</source>
        <translation>Результаты для %1 [%2 совпадений | %3 записей | %4%]</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Гид</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Руководство %1 не найдено, вместо него отображается руководство на английском языке.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 руководство не найдено!</translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Ручная запись</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Запись данных</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="227"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Добавить запись для %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Выберите дату и время</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Regular Heartbeat</source>
        <translation>Регулярное сердцебиение</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="134"/>
        <source>Enter SYS</source>
        <translation>Введите SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="156"/>
        <source>Enter DIA</source>
        <translation>Введите DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="178"/>
        <source>Enter BPM</source>
        <translation>Введите BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="200"/>
        <source>Irregular Heartbeat</source>
        <translation>Аритмия</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="255"/>
        <source>Enter Optional Comment</source>
        <translation>Введите необязательный комментарий</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="285"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Показать сообщение об успешном создании / удалении / изменении записи</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="303"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="320"/>
        <source>Create</source>
        <translation>Создавать</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="340"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="29"/>
        <location filename="../DialogRecord.cpp" line="97"/>
        <source>Modify</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="45"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Запись данных не может быть удалена! 

Запись для этой даты и времени не существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="51"/>
        <source>Data record successfully deleted.</source>
        <translation>Запись данных успешно удалена.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="61"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;SYS&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="71"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;DIA&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="80"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Пожалуйста, сначала введите допустимое значение для &quot;BPM&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="101"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Запись данных не может быть изменена! 

Запись для этой даты и времени не существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="107"/>
        <source>Data Record successfully modified.</source>
        <translation>Запись данных успешно изменена.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="116"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Запись данных не может быть создана! 

Запись для этой даты и времени уже существует.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="122"/>
        <source>Data Record successfully created.</source>
        <translation>Запись данных успешно создана.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation>Место расположения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation>Текущее местоположение</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation>Изменить местоположение</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Шифровать с помощью SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation>Шифрование</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation>Показать пароль</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation>Обязательная информация</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation>Мужской</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation>женский</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation>Возрастная группа</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation>год рождения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation>Высота</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation>Масса</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation>Импортировать плагины</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin...</source>
        <translation>Пожалуйста, выберите Плагин устройства ...</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation>Показать изображение устройства</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation>Показать руководство к устройству</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation>Открыть веб-сайт</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation>Поддержка</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation>Отправить электронное письмо</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation>Производитель</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation>Диаграмма</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>X-Axis Range</source>
        <translation>Диапазон оси X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="912"/>
        <source>Dynamic Scaling</source>
        <translation>Динамическое масштабирование</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="925"/>
        <source>Colored Areas</source>
        <translation>Цветные области</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="931"/>
        <source>Healthy Ranges</source>
        <translation>Здоровые диапазоны</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="948"/>
        <source>Symbols</source>
        <translation>Символы</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="954"/>
        <source>Interactive Objects</source>
        <translation>Интерактивные объекты</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="967"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="973"/>
        <source>Include Heartrate</source>
        <translation>Включить сердечный ритм</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="999"/>
        <location filename="../DialogSettings.ui" line="1274"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1087"/>
        <location filename="../DialogSettings.ui" line="1362"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1175"/>
        <location filename="../DialogSettings.ui" line="1450"/>
        <source>Heartrate</source>
        <translation>Частота сердцебиения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1547"/>
        <source>Table</source>
        <translation>Стол</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1578"/>
        <location filename="../DialogSettings.ui" line="1819"/>
        <source>Systolic Warnlevel</source>
        <translation>Уровень систолического предупреждения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1638"/>
        <location filename="../DialogSettings.ui" line="1879"/>
        <source>Diastolic Warnlevel</source>
        <translation>Уровень диастолического предупреждения</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1696"/>
        <location filename="../DialogSettings.ui" line="1937"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Уровень предупреждения о давлении</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1750"/>
        <location filename="../DialogSettings.ui" line="1991"/>
        <source>Heartrate Warnlevel</source>
        <translation>Уровень предупреждения сердечного ритма</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2056"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2062"/>
        <source>Bar Type</source>
        <translation>Тип бара</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2068"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Показывать медиану вместо столбцов средних значений</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2078"/>
        <source>Legend Type</source>
        <translation>Тип легенды</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2084"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Показывать значения в виде легенды вместо описаний</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2099"/>
        <source>Update</source>
        <translation>Обновлять</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2105"/>
        <source>Autostart</source>
        <translation>Автоматический старт</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2111"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Проверяйте наличие обновлений в Интернете при запуске программы</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2124"/>
        <source>Notification</source>
        <translation>Уведомление</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2130"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Всегда показывать результат после онлайн-проверки обновлений</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2146"/>
        <source>Save</source>
        <translation>Сохранять</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2163"/>
        <source>Reset</source>
        <translation>Перезагрузить</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2180"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="122"/>
        <source>Choose Database Location</source>
        <translation>Выберите расположение базы данных</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="158"/>
        <source>Could not display manual!

%1</source>
        <translation>Не удалось отобразить руководство! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="269"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Шифрование SQL невозможно включить без пароля, оно будет отключено!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="276"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Пожалуйста, введите действительные значения для дополнительной информации о пользователе 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="283"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Пожалуйста, введите действительные значения для дополнительной информации о пользователе 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="290"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Указанный возраст не соответствует возрастной группе пользователя 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="297"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Указанный возраст не соответствует возрастной группе пользователя 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="381"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="395"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="521"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Прервать установку и отменить все изменения?</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Онлайн-обновление</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Доступная версия</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Обновить размер файла</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Установленная версия</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Подробности</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation>Скачать</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation>Игнорировать</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="37"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>!!! ПРЕДУПРЕЖДЕНИЕ SSL - ПРОЧИТАЙТЕ ВНИМАТЕЛЬНО !!! 

Проблема (ы) сетевого подключения: 

%1 
Вы все равно хотите продолжить?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="54"/>
        <source>Downloading update failed!

%1</source>
        <translation>Не удалось загрузить обновление! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="54"/>
        <source>Checking update failed!

%1</source>
        <translation>Ошибка проверки обновления! 

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="132"/>
        <source>Unexpected response from update server!</source>
        <translation>Неожиданный ответ от сервера обновлений!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="159"/>
        <source>No new version found.</source>
        <translation>Новых версий не найдено.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="179"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download...</source>
        <translation>У обновления не ожидаемый размер! 

%L1:%L2 

Повторите загрузку ...</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Обновление сохранено в %1. 

Начать новую версию сейчас?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="196"/>
        <source>Could not start new version!</source>
        <translation>Не удалось запустить новую версию!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Не удалось сохранить обновление в %1! 

%2</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="239"/>
        <source>Really abort download?</source>
        <translation>Действительно прервать загрузку?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Универсальный менеджер артериального давления</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Таблица</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="3159"/>
        <source>Systolic</source>
        <translation>Систолическое</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="3160"/>
        <source>Diastolic</source>
        <translation>Диастолическое</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Арт.Давление</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="106"/>
        <location filename="../MainWindow.cpp" line="3161"/>
        <source>Heartrate</source>
        <translation>Частота сердцебиения</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Нерегулярное</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Invisible</source>
        <translation>Не Показывать</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="300"/>
        <source>Statistic View</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="395"/>
        <source>¼ Hourly</source>
        <translation>¼ Часа</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="430"/>
        <source>½ Hourly</source>
        <translation>½ Часа</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="462"/>
        <source>Hourly</source>
        <translation>Почасовой</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="497"/>
        <source>¼ Daily</source>
        <translation>6 Часов</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="529"/>
        <source>½ Daily</source>
        <translation>12 Часов</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="561"/>
        <source>Daily</source>
        <translation>День</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="596"/>
        <source>Weekly</source>
        <translation>Неделя</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="628"/>
        <source>Monthly</source>
        <translation>Месяц</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="660"/>
        <source>Quarterly</source>
        <translation>Квартал</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="692"/>
        <source>½ Yearly</source>
        <translation>6 Месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="724"/>
        <source>Yearly</source>
        <translation>Год</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="784"/>
        <source>Last 7 Days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="819"/>
        <source>Last 14 Days</source>
        <translation>Последние 14 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="851"/>
        <source>Last 21 Days</source>
        <translation>Последние 21 день</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="883"/>
        <source>Last 28 Days</source>
        <translation>Последние 28 дней</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="915"/>
        <source>Last 3 Months</source>
        <translation>Последние 3 месяца</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="947"/>
        <source>Last 6 Months</source>
        <translation>Последние 6 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="979"/>
        <source>Last 9 Months</source>
        <translation>Последние 9 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1011"/>
        <source>Last 12 Months</source>
        <translation>Последние 12 месяцев</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1043"/>
        <source>All Records</source>
        <translation>Все записи</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1090"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1094"/>
        <source>Print</source>
        <translation>Распечатать</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1114"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1118"/>
        <source>Make Donation</source>
        <translation>Сделать пожертвование</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1121"/>
        <source>Donation</source>
        <translation>Пожертвование</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1144"/>
        <source>Configuration</source>
        <translation>Конфигурация</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1148"/>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1166"/>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1183"/>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1187"/>
        <source>Import</source>
        <translation>Импорт</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1199"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1292"/>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1295"/>
        <location filename="../MainWindow.ui" line="1298"/>
        <source>Quit Program</source>
        <translation>Выйти из программы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>About Program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1322"/>
        <source>Guide</source>
        <translation>Гид</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <location filename="../MainWindow.ui" line="1328"/>
        <source>Show Guide</source>
        <translation>Показать руководство</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1337"/>
        <source>Update</source>
        <translation>Обновлять</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <location filename="../MainWindow.ui" line="1343"/>
        <source>Check Update</source>
        <translation>Проверить обновление</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1352"/>
        <source>Bugreport</source>
        <translation>Отчет об ошибке</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <location filename="../MainWindow.ui" line="1358"/>
        <source>Send Bugreport</source>
        <translation>Отправить отчет об ошибке</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1367"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1370"/>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Change Settings</source>
        <translation>Изменить настройки</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>From Device</source>
        <translation>С устройства</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <location filename="../MainWindow.ui" line="1391"/>
        <source>Import From Device</source>
        <translation>Импорт с устройства</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <source>From File</source>
        <translation>Из файла</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1403"/>
        <location filename="../MainWindow.ui" line="1406"/>
        <source>Import From File</source>
        <translation>Импортировать из файла</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>From Input</source>
        <translation>Ручной Ввод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1418"/>
        <location filename="../MainWindow.ui" line="1421"/>
        <source>Import From Input</source>
        <translation>Ручной Ввод</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <source>To CSV</source>
        <translation>В CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1433"/>
        <location filename="../MainWindow.ui" line="1436"/>
        <source>Export To CSV</source>
        <translation>Экспорт в CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>To XML</source>
        <translation>В XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Export To XML</source>
        <translation>Экспорт в XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>To JSON</source>
        <translation>В JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Export To JSON</source>
        <translation>Экспорт в JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To SQL</source>
        <translation>В SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To SQL</source>
        <translation>Экспорт в SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>Print Chart</source>
        <translation>Распечатать диаграмму</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Print Chart View</source>
        <translation>Распечатать диаграмму</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Print Table</source>
        <translation>Распечатать таблицу</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Print Table View</source>
        <translation>Печать в виде таблицы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Statistic</source>
        <translation>Статистика печати</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Statistic View</source>
        <translation>Просмотр статистики печати</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Preview Chart</source>
        <translation>Предварительный просмотр диаграммы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Preview Chart View</source>
        <translation>Предварительный просмотр диаграммы</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Preview Table</source>
        <translation>Таблица предварительного просмотра</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Preview Table View</source>
        <translation>Предварительный просмотр табличного представления</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Statistic</source>
        <translation>Предварительный просмотр статистики</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Statistic View</source>
        <translation>Предварительный просмотр статистики</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Clear All</source>
        <translation>Очистить все</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Clear User 1</source>
        <translation>Очистить пользователя 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear User 2</source>
        <translation>Очистить пользователя 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Switch User 1</source>
        <translation>Сменить пользователя 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1634"/>
        <location filename="../MainWindow.ui" line="1637"/>
        <location filename="../MainWindow.ui" line="1652"/>
        <location filename="../MainWindow.ui" line="1655"/>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="2999"/>
        <location filename="../MainWindow.cpp" line="3000"/>
        <location filename="../MainWindow.cpp" line="3001"/>
        <location filename="../MainWindow.cpp" line="3002"/>
        <location filename="../MainWindow.cpp" line="3190"/>
        <location filename="../MainWindow.cpp" line="3191"/>
        <location filename="../MainWindow.cpp" line="3192"/>
        <location filename="../MainWindow.cpp" line="3193"/>
        <source>Switch To %1</source>
        <translation>Перейти на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Switch User 2</source>
        <translation>Сменить пользователя 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1664"/>
        <source>Analysis</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1667"/>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Analyze Records</source>
        <translation>Анализировать записи</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Time Mode</source>
        <translation>Временной режим</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Donate via Paypal</source>
        <translation>Пожертвовать через Paypal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Donate via Liberapay</source>
        <translation>Пожертвовать через Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <source>Donate via Amazon</source>
        <translation>Пожертвовать через Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <source>Donate via SEPA</source>
        <translation>Пожертвовать через SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <location filename="../MainWindow.cpp" line="71"/>
        <location filename="../MainWindow.cpp" line="273"/>
        <location filename="../MainWindow.cpp" line="2999"/>
        <location filename="../MainWindow.cpp" line="3000"/>
        <location filename="../MainWindow.cpp" line="3190"/>
        <location filename="../MainWindow.cpp" line="3191"/>
        <source>User 1</source>
        <translation>Пользователь 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="72"/>
        <location filename="../MainWindow.cpp" line="73"/>
        <location filename="../MainWindow.cpp" line="283"/>
        <location filename="../MainWindow.cpp" line="3001"/>
        <location filename="../MainWindow.cpp" line="3002"/>
        <location filename="../MainWindow.cpp" line="3192"/>
        <location filename="../MainWindow.cpp" line="3193"/>
        <source>User 2</source>
        <translation>Пользователь 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="76"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3129"/>
        <location filename="../MainWindow.cpp" line="3130"/>
        <source>Records For Selected User</source>
        <translation>Записи для выбранного пользователя</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="84"/>
        <location filename="../MainWindow.cpp" line="85"/>
        <location filename="../MainWindow.cpp" line="3132"/>
        <location filename="../MainWindow.cpp" line="3133"/>
        <source>Select Date &amp; Time</source>
        <translation>Выберите дату и время</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="113"/>
        <location filename="../MainWindow.cpp" line="3163"/>
        <source>Systolic - Value Range</source>
        <translation>Систолическое - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="114"/>
        <location filename="../MainWindow.cpp" line="3164"/>
        <source>Diastolic - Value Range</source>
        <translation>Диастолическое - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="115"/>
        <location filename="../MainWindow.cpp" line="3165"/>
        <source>Heartrate - Value Range</source>
        <translation>Пульс - диапазон значений</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="3166"/>
        <source>Systolic - Target Area</source>
        <translation>Систолическое - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="118"/>
        <location filename="../MainWindow.cpp" line="3167"/>
        <source>Diastolic - Target Area</source>
        <translation>Диастолическое - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="119"/>
        <location filename="../MainWindow.cpp" line="3168"/>
        <source>Heartrate - Target Area</source>
        <translation>Частота пульса - Целевая область</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="706"/>
        <source>Measurements : %1  |  Irregular Heartbeat : %2</source>
        <translation>Измерения:%1 | Нерегулярное сердцебиение:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="707"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>СИС: Ø%1 / x̃%4 | ДИА: Ø%2 / x̃%5 | ПУЛЬС: Ø%3 / x̃%6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="711"/>
        <source>Measurements : 0  |  Irregular Heartbeat : 0</source>
        <translation>Измерения: 0 | Нерегулярное сердцебиение: 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="712"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>СИС: Ø 0 / x̃ 0 | ДИА: Ø 0 / x̃ 0 | ПУЛЬС: Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1147"/>
        <source>Athlete</source>
        <translation>Спортсмен</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1147"/>
        <source>To Low</source>
        <translation>К низкому</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1148"/>
        <source>Excellent</source>
        <translation>Отлично</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1148"/>
        <source>Optimal</source>
        <translation>Оптимально</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1149"/>
        <source>Great</source>
        <translation>Большой</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1149"/>
        <source>Normal</source>
        <translation>Нормальный</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1150"/>
        <source>Good</source>
        <translation>Хороший</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1150"/>
        <source>High Normal</source>
        <translation>Высокий Нормальный</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1151"/>
        <location filename="../MainWindow.cpp" line="1616"/>
        <location filename="../MainWindow.cpp" line="3172"/>
        <location filename="../MainWindow.cpp" line="3176"/>
        <location filename="../MainWindow.cpp" line="3180"/>
        <location filename="../MainWindow.h" line="278"/>
        <location filename="../MainWindow.h" line="282"/>
        <location filename="../MainWindow.h" line="286"/>
        <source>Average</source>
        <translation>Средний</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1151"/>
        <source>Hyper 1</source>
        <translation>Гипер 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1152"/>
        <source>Below Average</source>
        <translation>Ниже среднего</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1152"/>
        <source>Hyper 2</source>
        <translation>Гипер 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1153"/>
        <source>Poor</source>
        <translation>Бедные</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1153"/>
        <source>Hyper 3</source>
        <translation>Гипер 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1302"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Не удалось сканировать плагин импорта &quot;%1&quot;! 

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1315"/>
        <location filename="../MainWindow.cpp" line="1332"/>
        <location filename="../MainWindow.cpp" line="3139"/>
        <source>Switch Language to %1</source>
        <translation>Переключить язык на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1357"/>
        <location filename="../MainWindow.cpp" line="1372"/>
        <location filename="../MainWindow.cpp" line="3147"/>
        <source>Switch Theme to %1</source>
        <translation>Переключить тему на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1397"/>
        <location filename="../MainWindow.cpp" line="1417"/>
        <location filename="../MainWindow.cpp" line="3155"/>
        <source>Switch Style to %1</source>
        <translation>Переключить стиль на %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1485"/>
        <location filename="../MainWindow.cpp" line="1502"/>
        <location filename="../MainWindow.cpp" line="3821"/>
        <source>Delete record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1486"/>
        <location filename="../MainWindow.cpp" line="1509"/>
        <location filename="../MainWindow.cpp" line="3824"/>
        <source>Hide record</source>
        <translation>Скрыть запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1488"/>
        <location filename="../MainWindow.cpp" line="1513"/>
        <source>Edit record</source>
        <translation>Редактировать запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1504"/>
        <location filename="../MainWindow.cpp" line="3835"/>
        <source>Really delete selected record?</source>
        <translation>Действительно удалить выбранную запись?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1616"/>
        <location filename="../MainWindow.cpp" line="3173"/>
        <location filename="../MainWindow.cpp" line="3177"/>
        <location filename="../MainWindow.cpp" line="3181"/>
        <location filename="../MainWindow.h" line="279"/>
        <location filename="../MainWindow.h" line="283"/>
        <location filename="../MainWindow.h" line="287"/>
        <source>Median</source>
        <translation>Медиана</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1618"/>
        <source>Click to swap Average and Median</source>
        <translation>Нажмите, чтобы поменять местами Среднее и Медианное</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Click to swap Legend and Label</source>
        <translation>Нажмите, чтобы поменять местами легенду и метку</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1792"/>
        <source>No records to preview for selected time range!</source>
        <translation>Нет записей для предварительного просмотра за выбранный временной диапазон!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1826"/>
        <source>No records to print for selected time range!</source>
        <translation>Нет записей для печати за выбранный временной диапазон!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <location filename="../MainWindow.cpp" line="1932"/>
        <location filename="../MainWindow.cpp" line="2046"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation>Создано с помощью UBPM для 
Windows / Linux / MacOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1857"/>
        <location filename="../MainWindow.cpp" line="1933"/>
        <location filename="../MainWindow.cpp" line="2047"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Бесплатно и с открытым исходным кодом 
https: //codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1862"/>
        <location filename="../MainWindow.cpp" line="1938"/>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation>%1 (Возраст:%2, рост:%3, вес:%4)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1862"/>
        <location filename="../MainWindow.cpp" line="1938"/>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>User %1</source>
        <translation>Пользователь %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>DATE</source>
        <translation>ДАТА</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>TIME</source>
        <translation>ВРЕМЯ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <source>COMMENT</source>
        <translation>КОММЕНТАРИЙ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2144"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Импорт из CSV / XML / JSON / SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2144"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Файл CSV (* .csv) ;; Файл XML (* .xml) ;; Файл JSON (* .json) ;; Файл SQL (* .sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2148"/>
        <source>Could not open &quot;%1&quot;</source>
        <translation type="unfinished">Открыть &quot;%1&quot; не получилось</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="2909"/>
        <source>Successfully imported %1 records from %2.

     User 1 : %3
     User 2 : %4</source>
        <translation>Успешно импортировано %1 записей из %2. 

Пользователь 1:%3 
Пользователь 2:%4</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2196"/>
        <source>Skipped %1 invalid entries!</source>
        <translation>Пропущено %1 неправильныз записи!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2201"/>
        <location filename="../MainWindow.cpp" line="2913"/>
        <source>Skipped %1 duplicate entries!</source>
        <translation>Пропущено %1 повторяющихся записи!</translation>
    </message>
    <message>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="vanished">Не удалось открыть &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2539"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Не похоже на базу данных UBPM! 

Возможно, неправильные настройки шифрования / пароль?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2586"/>
        <source>Export to %1</source>
        <translation>Экспорт в %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2586"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 файл (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2617"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Не удалось создать &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2623"/>
        <source>The database is empty, no records to export!</source>
        <translation>База данных пуста, нет записей для экспорта!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2771"/>
        <source>Morning</source>
        <translation>Утро</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2771"/>
        <source>Afternoon</source>
        <translation>После полудня</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2779"/>
        <source>Week</source>
        <translation>Неделю</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2787"/>
        <source>Quarter</source>
        <translation>Четверть</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2791"/>
        <source>Half Year</source>
        <translation>Полгода</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2795"/>
        <source>Year</source>
        <translation>Год</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2843"/>
        <source>Really delete all records for user %1?</source>
        <translation>Действительно удалить все записи для пользователя %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2858"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Все записи для пользователя %1 удалены, а существующая база данных сохранена в «ubpm.sql.bak».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2963"/>
        <source>Really delete all records?</source>
        <translation>Действительно удалить все записи?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2974"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Все записи удалены, а существующая база данных «ubpm.sql» перемещена в корзину.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3057"/>
        <source>This program is Freeware and may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation>Эта программа является бесплатной и может быть установлена ​​и использована бесплатно для некоммерческого использования на любом количестве компьютеров без ограничений. Ответственность за любой ущерб, возникший в результате использования, исключается. Используйте на свой риск.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3072"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Пожалуйста, приобретите купон на желаемую сумму через свою учетную запись Amazon, выберите E-Mail to lazyt@mailbox.org в качестве способа доставки и укажите «UBPM» в качестве сообщения. 

Спасибо!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3089"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Пожалуйста, отправьте мне запрос по электронной почте на lazyt@mailbox.org, чтобы я мог предоставить вам информацию о моем текущем банковском счете. 

Спасибо!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3116"/>
        <source>Loading application translation failed!</source>
        <translation>Не удалось загрузить перевод приложения!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3125"/>
        <source>Loading Qt base translation failed!</source>
        <translation>Не удалось загрузить базовый перевод Qt!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3170"/>
        <location filename="../MainWindow.cpp" line="3174"/>
        <location filename="../MainWindow.cpp" line="3178"/>
        <location filename="../MainWindow.h" line="276"/>
        <location filename="../MainWindow.h" line="280"/>
        <location filename="../MainWindow.h" line="284"/>
        <source>Minimum</source>
        <translation>Минимум</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3171"/>
        <location filename="../MainWindow.cpp" line="3175"/>
        <location filename="../MainWindow.cpp" line="3179"/>
        <location filename="../MainWindow.h" line="277"/>
        <location filename="../MainWindow.h" line="281"/>
        <location filename="../MainWindow.h" line="285"/>
        <source>Maximum</source>
        <translation>Максимум</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3226"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Не удалось открыть файл темы &quot;%1&quot;! 

Причина:%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3770"/>
        <source>Dynamic Scaling</source>
        <translation>Динамическое масштабирование</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3771"/>
        <source>Colored Stripes</source>
        <translation>Цветные полосы</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3772"/>
        <source>Show Symbols</source>
        <translation>Показать символы</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3774"/>
        <source>Print Heartrate</source>
        <translation>Печать пульса</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3823"/>
        <source>Show record</source>
        <translation>Показать запись</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3856"/>
        <source>Show Median</source>
        <translation>Показать медиану</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3857"/>
        <source>Show Values</source>
        <translation>Показать значения</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3898"/>
        <source>Really quit program?</source>
        <translation>Действительно выйти из программы?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Универсальный менеджер артериального давления</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>Import a file</source>
        <translation>Ипортировать файл</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="14"/>
        <source>file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="23"/>
        <source>Error: Import file does not exist</source>
        <translation>Файл для импорта не существует</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="28"/>
        <source>Could not open &quot;%1&quot;</source>
        <translation>Открыть &quot;%1&quot; не получилось</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="31"/>
        <source>Successfully imported from &quot;%1&quot;

%2</source>
        <translation>Успешно импортированы данные из &quot;%1&quot;

        %2</translation>
    </message>
</context>
</TS>
