#ifndef DLGANALYSIS_H
#define DLGANALYSIS_H

#include "MainWindow.h"
#include "ui_DialogAnalysis.h"

class DialogAnalysis : public QDialog, private Ui::DialogAnalysis
{
	Q_OBJECT

public:

	explicit DialogAnalysis(QWidget*, int, struct SETTINGS*, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*);
	~DialogAnalysis();

private:

	QSqlDatabase sqldb;
	struct SETTINGS *settings;
	QVector <struct HEALTHDATA> *database1, *database2;

	bool createDB();
	void runQuery();

private slots:

	void initAfterShown();

	void on_comboBox_activated(int);
	void on_pushButton_clicked();

	void keyPressEvent(QKeyEvent*);
};

#endif // DLGANALYSIS_H
