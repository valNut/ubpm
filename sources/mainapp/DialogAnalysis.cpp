#include "DialogAnalysis.h"

DialogAnalysis::DialogAnalysis(QWidget *parent, int user, struct SETTINGS *psettings, QVector <struct HEALTHDATA> *pdatabase1, QVector <struct HEALTHDATA> *pdatabase2) : QDialog(parent)
{
	setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	settings = psettings;

	comboBox->addItem(QString("select * from U%1 where sys <= 129 and dia <= 84 and bpm <= 70").arg(user));
	comboBox->addItem(QString("select * from U%1 where sys > 129 or dia > 84 or bpm > 70").arg(user));
	comboBox->addItem(QString("select * from U%1 where ppr > 50").arg(user));
	comboBox->addItem(QString("select * from U%1 where ihb = true").arg(user));
	comboBox->addItem(QString("select * from U%1 where inv = true").arg(user));
	comboBox->addItem(QString("select * from U%1 where not msg = ''").arg(user));
	comboBox->addItem(QString("select * from U%1 where date >= '%2.01.01'").arg(user).arg(QDate::currentDate().year()));
	comboBox->addItem(QString("select * from U%1 where date between '%2.01.01' and '%2.12.31'").arg(user).arg(QDate::currentDate().year()));
	comboBox->addItem(QString("select * from U%1 where time >= '12:00:00'").arg(user));
	comboBox->addItem(QString("select * from U%1 where time between '06:00:00' and '09:00:00'").arg(user));

	tableWidget->setItemDelegateForColumn(0, new tableStyledItemDelegate);
	tableWidget->setItemDelegateForColumn(1, new tableStyledItemDelegate);
	tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	tableWidget->setMinimumHeight(10 * tableWidget->rowHeight(0) + tableWidget->horizontalHeader()->height());
	tableWidget->setRowCount(0);

	database1 = pdatabase1;
	database2 = pdatabase2;

	QTimer::singleShot(250, this, &DialogAnalysis::initAfterShown);
}

DialogAnalysis::~DialogAnalysis()
{
	sqldb.close();

	sqldb = QSqlDatabase();

	QSqlDatabase::removeDatabase("MEMDB");
}

void DialogAnalysis::initAfterShown()
{
	if(!createDB())
	{
		QMessageBox::warning(this, APPNAME, tr("Could not create memory database!\n\n%1").arg(sqldb.lastError().driverText()));

		close();
	}
	else
	{
		runQuery();
	}
}

bool DialogAnalysis::createDB()
{
	sqldb = QSqlDatabase::addDatabase("QSQLITE", "MEMDB");

	sqldb.setDatabaseName(":memory:");

	if(sqldb.open())
	{
		QSqlQuery query(sqldb);

		query.exec("CREATE TABLE 'U1' ('date' TEXT, 'time' TEXT, 'sys' INTEGER, 'dia' INTEGER, 'ppr' INTEGER, 'bpm' INTEGER, 'ihb' INTEGER, 'inv' INTEGER, 'msg' TEXT)");
		query.exec("CREATE TABLE 'U2' ('date' TEXT, 'time' TEXT, 'sys' INTEGER, 'dia' INTEGER, 'ppr' INTEGER, 'bpm' INTEGER, 'ihb' INTEGER, 'inv' INTEGER, 'msg' TEXT)");

		for(int i = 0; i < database1->count(); i++)
		{
			query.exec(QString("INSERT INTO 'U1' VALUES ('%1', '%2', %3, %4, %5, %6, %7, %8, '%9')").arg(QDateTime::fromMSecsSinceEpoch(database1->at(i).dts).toString("yyyy.MM.dd"), QDateTime::fromMSecsSinceEpoch(database1->at(i).dts).toString("hh:mm:ss")).arg(database1->at(i).sys).arg(database1->at(i).dia).arg(database1->at(i).sys - database1->at(i).dia).arg(database1->at(i).bpm).arg(database1->at(i).ihb).arg(database1->at(i).inv).arg(database1->at(i).msg));
		}

		for(int i = 0; i < database2->count(); i++)
		{
			query.exec(QString("INSERT INTO 'U2' VALUES ('%1', '%2', %3, %4, %5, %6, %7, %8,'%9')").arg(QDateTime::fromMSecsSinceEpoch(database2->at(i).dts).toString("yyyy.MM.dd"), QDateTime::fromMSecsSinceEpoch(database2->at(i).dts).toString("hh:mm:ss")).arg(database2->at(i).sys).arg(database2->at(i).dia).arg(database2->at(i).sys - database2->at(i).dia).arg(database2->at(i).bpm).arg(database2->at(i).ihb).arg(database2->at(i).inv).arg(database2->at(i).msg));
		}
	}
	else
	{
		return false;
	}

	return true;
}

void DialogAnalysis::runQuery()
{
	QTableWidgetItem *twi;
	QSqlQuery query(sqldb);
	int row = 0;
	int value;
	int user = comboBox->currentText().toUpper().contains("U1") ? 0 : 1;

	tableWidget->clearContents();
	tableWidget->setRowCount(0);

	query.exec(comboBox->currentText());

	while(query.next())
	{
		tableWidget->insertRow(row);

		if(query.record().contains("date"))
		{
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, QDateTime::fromString(query.value("date").toString(), "yyyy.MM.dd").date());
			twi->setTextAlignment(Qt::AlignCenter);
			tableWidget->setItem(row, 0, twi);
		}

		if(query.record().contains("time"))
		{
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, QDateTime::fromString(query.value("time").toString(), "hh:mm:ss").time());
			twi->setTextAlignment(Qt::AlignCenter);
			tableWidget->setItem(row, 1, twi);
		}

		if(query.record().contains("sys"))
		{
			value = query.value("sys").toInt();
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, value);
			twi->setTextAlignment(Qt::AlignCenter);
			if(value >= settings->table[user].warnsys) twi->setForeground(Qt::red);
			tableWidget->setItem(row, 2, twi);
		}

		if(query.record().contains("dia"))
		{
			value = query.value("dia").toInt();
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, value);
			twi->setTextAlignment(Qt::AlignCenter);
			if(value >= settings->table[user].warndia) twi->setForeground(Qt::red);
			tableWidget->setItem(row, 3, twi);
		}

		if(query.record().contains("ppr"))
		{
			value = query.value("ppr").toInt();
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, value);
			twi->setTextAlignment(Qt::AlignCenter);
			if(value > settings->table[user].warnppr) twi->setForeground(Qt::red);
			tableWidget->setItem(row, 4, twi);
		}

		if(query.record().contains("bpm"))
		{
			value = query.value("bpm").toInt();
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, value);
			twi->setTextAlignment(Qt::AlignCenter);
			if(value >= settings->table[user].warnbpm) twi->setForeground(Qt::red);
			tableWidget->setItem(row, 5, twi);
		}

		if(query.record().contains("ihb"))
		{
			value = query.value("ihb").toInt();
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, value);
			twi->setTextAlignment(Qt::AlignCenter);
			if(value) twi->setForeground(Qt::red);
			tableWidget->setItem(row, 6, twi);
		}

		if(query.record().contains("inv"))
		{
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, query.value("inv").toInt());
			twi->setTextAlignment(Qt::AlignCenter);
			tableWidget->setItem(row, 7, twi);
		}

		if(query.record().contains("msg"))
		{
			twi = new QTableWidgetItem();
			twi->setData(Qt::DisplayRole, query.value("msg").toString());
			twi->setTextAlignment(Qt::AlignCenter);
			tableWidget->setItem(row, 8, twi);
		}

		row++;
	}

	if(!row)
	{
		groupBox_results->setTitle(tr("Results"));

		QMessageBox::information(this, APPNAME, tr("No results for this query found!"));
	}
	else
	{
		int count = query.record().field(0).tableName() == "U1" ? database1->count() : database2->count();

		groupBox_results->setTitle(tr("Results for %1 [ %2 Matches | %3 Records | %4% ]").arg(settings->user[user].name).arg(row).arg(count).arg(row*100.0 / count, 0, 'f', 2));
	}
}

void DialogAnalysis::on_comboBox_activated(int /*index*/)
{
	runQuery();
}

void DialogAnalysis::on_pushButton_clicked()
{
	close();
}

void DialogAnalysis::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		DialogHelp(reinterpret_cast<QWidget*>(parent()), "01-05").exec();
	}

	QDialog::keyPressEvent(ke);
}
