# **UBPM - Universal Blood Pressure Manager**

[![](https://badgen.net/badge/Framework/Qt5/orange)](https://qt.io "Get Info")
[![](https://badgen.net/badge/Platform/WIN%20|%20LIN%20|%20MAC/blue)](https://codeberg.org/lazyt/ubpm/releases "Download Release")
[![](https://badgen.net/badge/License/GPL-3.0/pink)](https://codeberg.org/lazyt/ubpm/src/branch/master/LICENSE "Show License")
[![](https://badgen.net/badge/Paypal/Sponsor%20Beer%20Once/red)](https://paypal.me/lazyt "Support Developer")
[![](https://badgen.net/badge/Liberapay/Sponsor%20Beer%20Weekly/red)](https://liberapay.com/lazyt/donate "Support Developer")

## Description

Tired of original software supplied by the manufacturer of your blood pressure monitor because it's only available for Windows and requires an internet connection for uploading your private health data into the cloud? Then try UBPM for free and use it on Windows, Linux and MacOS!

The current version supports the following features:

* import data from manual input, file (csv, json, xml, sql) or directly from supported blood pressure monitors
* export data to csv, json, xml or sql format
* view and print data as chart, table or statistics
* analyze data via sql queries
* plugin interface for blood pressure monitors with computer interface
* online updater
* context sensitive help via F1 key
* style gui via css files
* multi language (English, German and Russian)
* cross platform (same look & feel on Windows, Linux, MacOS)

#### Supported blood pressure monitors

Manufacturer | Model | Description | Interface
:---:|:---:|:---:|:---:
[OMRON Corporation](https://omronhealthcare.com/blood-pressure) | HEM-7322U | M6 Comfort IT, M500 IT | USB
[OMRON Corporation](https://omronhealthcare.com/blood-pressure) | HEM-7131U | M3 IT, M400 IT | USB

Please help to add more devices! Read the "Device Plugins" section of the guide and take a look at the plugin [source](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7322u) to get started.

## Screenshots

Main window with some records...

![Chart View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartview.png "Chart View")
![Table View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableview.png "Table View")
![Stats View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsview.png "Stats View")

Print records for your doctor...

![Chart Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartprint.png "Chart Print")
![Table Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableprint.png "Table Print")
![Stats Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsprint.png "Stats Print")

Create/Modify manual record(s)...

![Input](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/input.png "Input")

Import from supported device...

![Import](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/import.gif "Import")

Analyze records via sql queries...

![Analysis](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/analysis.png "Analysis")

Configure your preferred settings...

![Settings](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/settings.png "Settings")

Press F1 to get help for the current context...

![Guide](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/guide.png "Guide")

## Download

Download the latest version for your operating system. All 3 files (exe, dmg, AppImage) contain everything to run UBPM on the target platform without installing anything.

* [Windows (exe)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an [7zip](https://www.7-zip.org) self extracting archive. It will be automatically extracted to "%temp%\7zxxxxxxxx" and after that the "ubpm.exe" is started. You can copy this directory or extract the file with 7z if you want the content.

**IMPORTANT NOTE: some virus scanners classify UBPM as malicious! This is due to the use of [7zsfx](https://www.7-zip.org/a/lzma1900.7z). If you don't trust me rename exe to zip, unpack and scan that directory, no warnings should appear for UBPM itself.**

* [MacOS (dmg)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an Apple disc image. You can mount and run or copy the application.

* [Linux (AppImage)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an [AppImage](https://appimage.org) package. Don't forget to "chmod +x *.AppImage" after download and execute. You can use the parameter "--appimage-extract" if you want the content.

For better system integration it's recommended to install [AppImageD](https://github.com/AppImage/appimaged) or [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher).

## Build from Source

Download the latest [Qt Online Installer](https://qt.io/download-open-source) and install Qt 5.15.x, Qt Charts and OpenSSL binaries first.

- Checkout the source code via git (or download and extract the [zip](https://codeberg.org/LazyT/ubpm/archive/master.zip))

		git clone https://codeberg.org/lazyt/ubpm

- Change into the ubpm sources directory and generate the Makefile

		cd ubpm/sources && qmake

- Compile the source code

		make

- Optional create a release package (installs nothing on the build system)

		make install

### Docker builds for producing AppImage
AppImage builds have to be done on a system with GLibc version no newer than 2.23 in order to ensure broadest compatible range of distribution. Included Dockerfile can be used to run the build while building on systems with newer GLibc.

- Build and tag the Docker image (from the root of the repository):
```sh
		docker build . -t ubpm-build:ubuntu-16.04
```

- To run the actual build:
```sh
		docker run --rm -it -v <fully-qualified path to the solution dir>:/code ubpm-build:ubuntu-16.04 /code/build.sh   
```

- The resulting AppImage will be located in `./sources/mainapp/ubpm-*.AppImage`


## Credits

UBPM is based on

* [Qt](https://www.qt.io)
* [hidapi](https://github.com/libusb/hidapi)

Thanks for this great software! :+1:
