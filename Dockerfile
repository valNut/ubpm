FROM ubuntu:16.04

ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get -y install build-essential libudev-dev libgl1-mesa-dev libegl1-mesa-dev libglib2.0-0 libgssapi-krb5-2 \
    wget zip libfontconfig1 libxcb* libxkbcommon-x11-0 libdbus-1-3 libcups2 libodbc1 libpq5 software-properties-common zlib1g-dev 

RUN add-apt-repository ppa:deadsnakes/ppa && apt-get update && apt-get -y install python3.8 python3.8-distutils python3.8-venv
RUN python3.8 -m ensurepip && python3.8 -m pip install aqtinstall && aqt install --outputdir /opt 5.15.2 linux desktop -m all
RUN ln -s /opt/5.15.2/gcc_64 /usr/lib/qt5 

RUN wget https://www.openssl.org/source/openssl-1.1.1j.tar.gz -O - | tar -xz 
WORKDIR /openssl-1.1.1j
RUN ./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl && make && make install
RUN mkdir -p /opt/Tools/OpenSSL/binary/ && ln -s /usr/local/openssl/lib  /opt/Tools/OpenSSL/binary 

RUN wget -O /usr/sbin/linuxdeployqt-continuous-x86_64.AppImage https://github.com/probonopd/linuxdeployqt/releases/download/7/linuxdeployqt-7-x86_64.AppImage &&\
    chmod +x /usr/sbin/linuxdeployqt-continuous-x86_64.AppImage

ENV PATH="${PATH}:/opt/5.15.2/gcc_64/bin"

WORKDIR /code